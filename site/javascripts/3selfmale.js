AmCharts.makeChart("3selfmale",
	{
		"type": "serial",
		"categoryField": "category",
		"angle": 30,
		"depth3D": 30,
		"startDuration": 1,
		"theme": "black",
		"categoryAxis": {
			"gridPosition": "start"
		},
		"trendLines": [],
		"graphs": [
			{
				"balloonText": "[[title]]: [[value]]",
				"fillAlphas": 0.99,
				"id": "AmGraph-5",
				"title": "Self Rating",
				"type": "column",
				"valueField": "column-1"
			},
			{
				"balloonText": "[[title]]: [[value]]",
				"fillAlphas": 1,
				"id": "",
				"title": "Expectations of Others' Rating",
				"type": "column",
				"valueField": "column-2"
			},
			{
				"balloonText": "[[title]]: [[value]]",
				"fillAlphas": 1,
				"id": "AmGraph-7",
				"title": "Other's Rating",
				"type": "column",
				"valueField": "column-3"
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"title": "Rating"
			}
		],
		"allLabels": [],
		"balloon": {},
		"legend": {
			"enabled": true,
			"align": "center",
			"maxColumns": 2,
			"useGraphSettings": true
		},
		"titles": [
			{
				"id": "Title-1",
				"size": 15,
				"text": "Self Judgment and Reflection (Male)"
			}
		],
		"dataProvider": [
			{
				"category": "Ambitious",
				"column-1": "7.524783",
				"column-2": "7.489170",
				"column-3": "5.934091"
			},
			{
				"category": "Attractive",
				"column-1": "6.951636",
				"column-2": "6.865550",
				"column-3": "7.156348"
			},
			{
				"category": "Fun",
				"column-1": "7.517084",
				"column-2": "7.350633",
				"column-3": "7.485017"
			},
			{
				"category": "Intelligent",
				"column-1": "8.486526",
				"column-2": "8.342869",
				"column-3": "6.326965"
			},
			{
				"category": "Shared Interests",
				"column-1": "",
				"column-2": "",
				"column-3": "7.008968"
			},
			{
				"category": "Sincere",
				"column-1": "8.133061",
				"column-2": "7.737229",
				"column-3": "5.420575"
			}
		]
	}
);