			AmCharts.makeChart("3male",
				{
					"type": "serial",
					"categoryField": "category",
					"angle": 30,
					"depth3D": 30,
					"startDuration": 1,
					"theme": "black",
					"categoryAxis": {
						"gridPosition": "start"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]]: [[value]]",
							"fillAlphas": 0.99,
							"id": "AmGraph-5",
							"title": "Male Expectations",
							"type": "column",
							"valueField": "column-1"
						},
						{
							"balloonText": "[[title]]: [[value]]",
							"fillAlphas": 1,
							"id": "",
							"title": "Female Expectations",
							"type": "column",
							"valueField": "column-2"
						},
						{
							"balloonText": "[[title]]: [[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-7",
							"title": "Pre-Study Answers",
							"type": "column",
							"valueField": "column-3"
						},
						{
							"balloonText": "[[title]]: [[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-8",
							"title": "Post-Study Answers",
							"type": "column",
							"valueField": "column-4"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "Rating"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true,
						"align": "center",
						"maxColumns": 2,
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": "Trait Importance to Males"
						}
					],
					"dataProvider": [
						{
							"category": "Ambitious",
							"column-1": "7.690080",
							"column-2": "9.230638",
							"column-3": "8.823956",
							"column-4": "5.855876"
						},
						{
							"category": "Attractive",
							"column-1": "28.075786",
							"column-2": "35.694349",
							"column-3": "27.248800",
							"column-4": "36.522173"
						},
						{
							"category": "Fun",
							"column-1": "16.204251",
							"column-2": "18.733510",
							"column-3": "17.600839",
							"column-4": "17.829268"
						},
						{
							"category": "Intelligent",
							"column-1": "12.163894",
							"column-2": "12.532022",
							"column-3": "16.379600",
							"column-4": "17.087583"
						},
						{
							"category": "Shared Interests",
							"column-1": "10.871534",
							"column-2": "12.645113",
							"column-3": "10.982271",
							"column-4": "10.297118"
						},
						{
							"category": "Sincere",
							"column-1": "10.774184",
							"column-2": "11.343646",
							"column-3": "19.387418",
							"column-4": "12.651885"
						}
					]
				}
			);