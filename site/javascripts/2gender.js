AmCharts.makeChart("2gender",
				{
					"type": "serial",
					"categoryField": "category",
					"angle": 30,
					"depth3D": 30,
					"startDuration": 1,
					"theme": "black",
					"categoryAxis": {
						"gridPosition": "start"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[category]] of [[title]]: [[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"title": "Female",
							"type": "column",
							"valueField": "column-1"
						},
						{
							"balloonText": "[[category]] of [[title]]: [[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-2",
							"title": "Male",
							"type": "column",
							"valueField": "column-2"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "Rating"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true,
						"align": "center",
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": "Trait Ratings by Gender"
						}
					],
					"dataProvider": [
						{
							"category": "Ambitious",
							"column-1": "6.604591",
							"column-2": "6.952773"
						},
						{
							"category": "Attractive",
							"column-1": "6.461401",
							"column-2": "5.919422"
						},
						{
							"category": "Fun",
							"column-1": "6.520164",
							"column-2": "6.280555"
						},
						{
							"category": "Intelligent",
							"column-1": "7.291202",
							"column-2": "7.447362"
						},
						{
							"category": "Shared Interests",
							"column-1": "5.541148",
							"column-2": "5.407012"
						},
						{
							"category": "Sincere",
							"column-1": "7.251053",
							"column-2": "7.099778"
						}
					]
				}
			);