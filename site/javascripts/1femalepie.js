AmCharts.makeChart("1femalepie",
	{
		"type": "pie",
		"angle": 12,
		"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
		"depth3D": 15,
		"innerRadius": "40%",
		"labelText": "[[title]]: [[value]] ([[percents]]%)",
		"titleField": "category",
		"valueField": "column-1",
		"theme": "dark",
		"allLabels": [],
		"balloon": {},
		"legend": {
			"enabled": true,
			"align": "center",
			"markerType": "circle",
			"valueWidth": 150
		},
		"titles": [
			{
				"id": "Title-1",
				"text": "Relative Avg. Trait Importance atings (Female)"
			}
		],
		"dataProvider": [
			{
				"category": "Ambitious",
				"column-1": "12.818476"
			},
			{
				"category": "Attractive",
				"column-1": "18.020372"
			},
			{
				"category": "Fun",
				"column-1": "17.299108"
			},
			{
				"category": "Intelligent",
				"column-1": "18.222230"
			},
			{
				"category": "Shared Interests",
				"column-1": "12.697836"
			},
			{
				"category": "Sincere",
				"column-1": "20.971004"
			}
		]
	}
);