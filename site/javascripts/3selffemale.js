AmCharts.makeChart("3selffemale",
				{
					"type": "serial",
					"categoryField": "category",
					"angle": 30,
					"depth3D": 30,
					"startDuration": 1,
					"theme": "dark",
					"categoryAxis": {
						"gridPosition": "start"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]]: [[value]]",
							"fillAlphas": 0.99,
							"id": "AmGraph-5",
							"title": "Self Rating",
							"type": "column",
							"valueField": "column-1"
						},
						{
							"balloonText": "[[title]]: [[value]]",
							"fillAlphas": 1,
							"id": "",
							"title": "Expectations of Others' Rating",
							"type": "column",
							"valueField": "column-2"
						},
						{
							"balloonText": "[[title]]: [[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-7",
							"title": "Other's Rating",
							"type": "column",
							"valueField": "column-3"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "Rating"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true,
						"align": "center",
						"maxColumns": 2,
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": "Self Judgment and Reflection (Female)"
						}
					],
					"dataProvider": [
						{
							"category": "Ambitious",
							"column-1": "7.632499",
							"column-2": "7.745425",
							"column-3": "6.474446"
						},
						{
							"category": "Attractive",
							"column-1": "7.219092",
							"column-2": "7.017893",
							"column-3": "7.283696"
						},
						{
							"category": "Fun",
							"column-1": "7.893612",
							"column-2": "7.501423",
							"column-3": "7.311466"
						},
						{
							"category": "Intelligent",
							"column-1": "8.320622",
							"column-2": "8.226108",
							"column-3": "6.543318"
						},
						{
							"category": "Shared Interests",
							"column-1": "",
							"column-2": "",
							"column-3": "6.633117"
						},
						{
							"category": "Sincere",
							"column-1": "8.458343",
							"column-2": "8.116307",
							"column-3": "5.576278"
						}
					]
				}
			);