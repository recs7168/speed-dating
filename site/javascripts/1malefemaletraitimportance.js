AmCharts.makeChart("1genderbar",
				{
					"type": "serial",
					"categoryField": "category",
					"angle": 30,
					"depth3D": 30,
					"startDuration": 1,
					"theme": "black",
					"categoryAxis": {
						"gridPosition": "start"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"title": "Female",
							"type": "column",
							"valueField": "column-1"
						},
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-2",
							"title": "Male",
							"type": "column",
							"valueField": "column-2"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "Rating of Importance"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true,
						"align": "center",
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": "Trait Importance by Gender"
						}
					],
					"dataProvider": [
						{
							"category": "Ambitious",
							"column-1": "12.818476",
							"column-2": "8.823956"
						},
						{
							"category": "Attractive",
							"column-1": "18.020372",
							"column-2": "27.2488"
						},
						{
							"category": "Fun",
							"column-1": "17.299108",
							"column-2": "17.600839"
						},
						{
							"category": "Intelligent",
							"column-1": "18.22223",
							"column-2": "16.3796"
						},
						{
							"category": "Shared Interests",
							"column-1": "12.697836",
							"column-2": "10.982271"
						},
						{
							"category": "Sincere",
							"column-1": "20.971004",
							"column-2": "19.387418"
						}
					]
				}
			);