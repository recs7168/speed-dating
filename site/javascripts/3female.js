AmCharts.makeChart("3female",
				{
					"type": "serial",
					"categoryField": "category",
					"angle": 30,
					"depth3D": 30,
					"startDuration": 1,
					"theme": "dark",
					"categoryAxis": {
						"gridPosition": "start"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]]: [[value]]",
							"fillAlphas": 0.99,
							"id": "AmGraph-5",
							"title": "Male Expectations",
							"type": "column",
							"valueField": "column-1"
						},
						{
							"balloonText": "[[title]]: [[value]]",
							"fillAlphas": 1,
							"id": "",
							"title": "Female Expectations",
							"type": "column",
							"valueField": "column-2"
						},
						{
							"balloonText": "[[title]]: [[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-7",
							"title": "Pre-Study Answers",
							"type": "column",
							"valueField": "column-3"
						},
						{
							"balloonText": "[[title]]: [[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-8",
							"title": "Post-Study Answers",
							"type": "column",
							"valueField": "column-4"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "Rating"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true,
						"align": "center",
						"maxColumns": 2,
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": "Trait Importance to Females"
						}
					],
					"dataProvider": [
						{
							"category": "Ambitious",
							"column-1": "14.234815",
							"column-2": "11.872032",
							"column-3": "12.818476",
							"column-4": "9.417415"
						},
						{
							"category": "Attractive",
							"column-1": "25.092631",
							"column-2": "24.711378",
							"column-3": "18.020372",
							"column-4": "27.126571"
						},
						{
							"category": "Fun",
							"column-1": "18.115379",
							"column-2": "14.928770",
							"column-3": "17.299108",
							"column-4": "15.275583"
						},
						{
							"category": "Intelligent",
							"column-1": "16.279633",
							"column-2": "13.109158",
							"column-3": "18.222230",
							"column-4": "16.349192"
						},
						{
							"category": "Shared Interests",
							"column-1": "11.071924",
							"column-2": "11.159267",
							"column-3": "12.697836",
							"column-4": "13.754937"
						},
						{
							"category": "Sincere",
							"column-1": "15.181078",
							"column-2": "11.369103",
							"column-3": "20.971004",
							"column-4": "18.085278"
						}
					]
				}
			);