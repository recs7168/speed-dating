AmCharts.makeChart("2race",
				{
					"type": "serial",
					"categoryField": "category",
					"angle": 30,
					"depth3D": 30,
					"startDuration": 1,
					"theme": "black",
					"categoryAxis": {
						"gridPosition": "start"
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[category]] of [[title]]: [[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"title": "Black",
							"type": "column",
							"valueField": "column-1"
						},
						{
							"balloonText": "[[category]] of [[title]]: [[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-2",
							"title": "White",
							"type": "column",
							"valueField": "column-2"
						},
						{
							"balloonText": "[[category]] of [[title]]: [[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-3",
							"title": "Latino",
							"type": "column",
							"valueField": "column-3"
						},
						{
							"balloonText": "[[category]] of [[title]]: [[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-4",
							"title": "Asian",
							"type": "column",
							"valueField": "column-4"
						},
						{
							"balloonText": "[[category]] of [[title]]: [[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-5",
							"title": "Other",
							"type": "column",
							"valueField": "column-5"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "Axis title"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true,
						"useGraphSettings": true
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": "Trait Ratings by Race"
						}
					],
					"dataProvider": [
						{
							"category": "Ambitious",
							"column-1": "6.832061",
							"column-2": "6.764802",
							"column-3": "6.837379",
							"column-4": "6.769907",
							"column-5": "6.780083"
						},
						{
							"category": "Attractive",
							"column-1": "6.063260",
							"column-2": "6.372944",
							"column-3": "6.527090",
							"column-4": "5.693841",
							"column-5": "5.998047"
						},
						{
							"category": "Fun",
							"column-1": "6.689487",
							"column-2": "6.458582",
							"column-3": "6.860502",
							"column-4": "6.005294",
							"column-5": "6.485030"
						},
						{
							"category": "Intelligent",
							"column-1": "7.290954",
							"column-2": "7.353238",
							"column-3": "7.392969",
							"column-4": "7.398163",
							"column-5": "7.424361"
						},
						{
							"category": "Shared Interests",
							"column-1": "5.452128",
							"column-2": "5.580929",
							"column-3": "5.715517",
							"column-4": "5.108621",
							"column-5": "5.523758"
						},
						{
							"category": "Sincere",
							"column-1": "7.213592",
							"column-2": "7.124945",
							"column-3": "7.362150",
							"column-4": "7.187533",
							"column-5": "7.300589"
						}
					]
				}
			);