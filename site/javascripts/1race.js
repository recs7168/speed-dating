AmCharts.makeChart("1race",
				{
					"type": "serial",
					"categoryField": "category",
					"angle": 30,
					"depth3D": 30,
					"startDuration": 1,
					"accessible": false,
					"tapToActivate": false,
					"theme": "black",
					"categoryAxis": {
						"gridPosition": "start"
					},
					"chartScrollbar": {
						"enabled": true
					},
					"trendLines": [],
					"graphs": [
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"title": "Asian",
							"type": "column",
							"valueField": ""
						},
						{
							"balloonText": "[[title]] of [[category]]:[[value]]",
							"fillAlphas": 1,
							"id": "AmGraph-2",
							"title": "Black",
							"type": "column",
							"valueField": "column-2"
						},
						{
							"fillAlphas": 1,
							"id": "AmGraph-3",
							"lineAlpha": 1,
							"tabIndex": -1,
							"title": "Latino",
							"type": "column",
							"valueField": "column-3"
						},
						{
							"fillAlphas": 1,
							"id": "AmGraph-4",
							"title": "Other",
							"type": "column",
							"valueField": "column-4"
						},
						{
							"fillAlphas": 1,
							"id": "AmGraph-5",
							"title": "White",
							"type": "column",
							"valueField": "column-5"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "Importance Rating"
						}
					],
					"allLabels": [],
					"balloon": {},
					"legend": {
						"enabled": true,
						"useGraphSettings": true,
						"valueWidth": 51
					},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": "Trait Importance Ratings (by Race)"
						}
					],
					"dataProvider": [
						{
							"category": "Ambitious",
							"column-2": "12.741923",
							"column-3": "11.822619",
							"column-4": "10.107568",
							"column-5": "10.500000",
							"": "10.994741"
						},
						{
							"category": "Attractive",
							"column-2": "21.792692",
							"column-3": "21.645238",
							"column-4": "22.369459",
							"column-5": "23.470924",
							"": "21.513456"
						},
						{
							"category": "Fun",
							"column-2": "17.443846",
							"column-3": "15.981667",
							"column-4": "18.046486",
							"column-5": "18.033267",
							"": "16.440889"
						},
						{
							"category": "Intelligent",
							"column-2": "16.729231",
							"column-3": "18.032381",
							"column-4": "18.229730",
							"column-5": "16.306700",
							"": "19.106029"
						},
						{
							"category": "Shared Interests",
							"column-2": "11.502692",
							"column-3": "11.989024",
							"column-4": "10.803243",
							"column-5": "11.199702",
							"": "13.544667"
						},
						{
							"category": "Sincere",
							"column-2": "19.443846",
							"column-3": "21.601667",
							"column-4": "20.173514",
							"column-5": "20.693201",
							"": "18.701985"
						}
					]
				}
			);