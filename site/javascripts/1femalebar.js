AmCharts.makeChart("1femalebar",
	{
		"type": "serial",
		"categoryField": "category",
		"startDuration": 1,
		"theme": "dark",
		"categoryAxis": {
			"gridPosition": "start"
		},
		"trendLines": [],
		"graphs": [
			{
				"colorField": "color",
				"fillAlphas": 1,
				"id": "AmGraph-1",
				"lineColorField": "color",
				"title": "graph 1",
				"type": "column",
				"valueField": "column-1"
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"title": "Importance Rating"
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [
			{
				"id": "Title-1",
				"size": 15,
				"text": "Avg. Trait Importance Ratings (Female)"
			}
		],
		"dataProvider": [
			{
				"category": "Ambitious",
				"column-1": "12.818476",
				"color": "#495fba"
			},
			{
				"category": "Attractive",
				"column-1": "18.020372",
				"color": "#e8d685"
			},
			{
				"category": "Fun",
				"column-1": "17.299108",
				"color": "#ae85c9"
			},
			{
				"category": "Intelligent",
				"column-1": "18.22223",
				"color": "#c9f0e1"
			},
			{
				"category": "Shared Interests",
				"column-1": "12.697836",
				"color": "#d48652"
			},
			{
				"category": "Sincere",
				"column-1": "20.971004",
				"color": "#629b6d"
			}
		]
	}
);