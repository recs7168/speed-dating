AmCharts.makeChart("1malebyrace",
	{
		"type": "serial",
		"categoryField": "category",
		"angle": 30,
		"depth3D": 30,
		"startDuration": 1,
		"theme": "black",
		"categoryAxis": {
			"gridPosition": "start"
		},
		"trendLines": [],
		"graphs": [
			{
				"balloonText": "[[title]] Rating from '[[category]]':[[value]]",
				"fillAlphas": 1,
				"id": "AmGraph-1",
				"title": "Attractive",
				"type": "column",
				"valueField": "Attractive"
			},
			{
				"balloonText": "[[title]] Rating from '[[category]]':[[value]]",
				"fillAlphas": 1,
				"id": "AmGraph-2",
				"title": "Intelligent",
				"type": "column",
				"valueField": "Intelligent"
			},
			{
				"balloonText": "[[title]] Rating from '[[category]]':[[value]]",
				"fillAlphas": 1,
				"id": "AmGraph-4",
				"title": "Fun",
				"type": "column",
				"valueField": "Fun"
			},
			{
				"balloonText": "[[title]] Rating from '[[category]]':[[value]]",
				"fillAlphas": 1,
				"id": "AmGraph-5",
				"title": "Sincere",
				"type": "column",
				"valueField": "Sincere"
			},
			{
				"balloonText": "[[title]] Rating from '[[category]]':[[value]]",
				"fillAlphas": 1,
				"id": "AmGraph-6",
				"title": "Ambitious",
				"type": "column",
				"valueField": "Ambitious"
			},
			{
				"balloonText": "[[title]] Rating from '[[category]]':[[value]]",
				"fillAlphas": 1,
				"id": "AmGraph-7",
				"title": "Shared Interests",
				"type": "column",
				"valueField": "Shared Interests"
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"title": "Rating"
			}
		],
		"allLabels": [],
		"balloon": {
			"animationDuration": 0
		},
		"legend": {
			"enabled": true,
			"maxColumns": 3,
			"rollOverGraphAlpha": 0,
			"useGraphSettings": true
		},
		"titles": [
			{
				"id": "Title-1",
				"size": 15,
				"text": "Trait Importance Ratings (Males by Race)"
			}
		],
		"dataProvider": [
			{
				"category": "Asian",
				"Attractive": "25.862462",
				"Intelligent": "18.656154",
				"Fun": "16.318125",
				"Sincere": "16.800923",
				"Ambitious": "9.814062",
				"Shared Interests": "13.152656"
			},
			{
				"category": "Black",
				"Attractive": "25.700000",
				"Intelligent": "17.800000",
				"Fun": "17.000000",
				"Sincere": "20.500000",
				"Ambitious": "8.200000",
				"Shared Interests": "9.900000"
			},
			{
				"category": "Latino",
				"Attractive": "25.125882",
				"Intelligent": "17.027647",
				"Fun": "16.155294",
				"Sincere": "20.331765",
				"Ambitious": "10.951176",
				"Shared Interests": "13.233529"
			},
			{
				"category": "Other",
				"Attractive": "25.353810",
				"Intelligent": "17.432857",
				"Fun": "19.210952",
				"Sincere": "19.909048",
				"Ambitious": "9.217619",
				"Shared Interests": "8.399524"
			},
			{
				"category": "White",
				"Attractive": "28.369074",
				"Intelligent": "15.173951",
				"Fun": "18.087654",
				"Sincere": "20.189815",
				"Ambitious": "8.193168",
				"Shared Interests": "10.285901"
			}
		]
	}
);