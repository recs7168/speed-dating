AmCharts.makeChart("1malepie",
	{
		"type": "pie",
		"angle": 12,
		"balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
		"depth3D": 15,
		"innerRadius": "40%",
		"labelText": "[[title]]: [[value]] ([[percents]]%)",
		"titleField": "category",
		"valueField": "column-1",
		"theme": "black",
		"allLabels": [],
		"balloon": {},
		"legend": {
			"enabled": true,
			"align": "center",
			"markerType": "circle",
			"valueWidth": 150
		},
		"titles": [
			{
				"id": "Title-1",
				"text": "Relative Avg. Trait Importance Ratings (Male)"
			}
		],
		"dataProvider": [
			{
				"category": "Ambitious",
				"column-1": "8.823956"
			},
			{
				"category": "Attractive",
				"column-1": "27.248800"
			},
			{
				"category": "Fun",
				"column-1": "17.600839"
			},
			{
				"category": "Intelligent",
				"column-1": "16.379600"
			},
			{
				"category": "Shared Interests",
				"column-1": "10.982271"
			},
			{
				"category": "Sincere",
				"column-1": "19.387418"
			}
		]
	}
);