AmCharts.makeChart("1overallbar",
	{
		"type": "serial",
		"categoryField": "category",
		"startDuration": 1,
		"theme": "black",
		"categoryAxis": {
			"gridPosition": "start"
		},
		"trendLines": [],
		"graphs": [
			{
				"colorField": "color",
				"fillAlphas": 1,
				"id": "AmGraph-1",
				"lineColorField": "color",
				"title": "graph 1",
				"type": "column",
				"valueField": "column-1"
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"title": "Avg. Importance Rating"
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [
			{
				"id": "Title-1",
				"size": 15,
				"text": "Avg. Trait Importances"
			}
		],
		"dataProvider": [
			{
				"category": "Attractive",
				"column-1": "22.685478",
				"color": "#de4c4f"
			},
			{
				"category": "Intelligent",
				"column-1": "17.290754",
				"color": "#d8854f"
			},
			{
				"category": "Sincere",
				"column-1": "20.170478",
				"color": "#eea638"
			},
			{
				"category": "Fun",
				"column-1": "17.451363",
				"color": "#a7a737"
			},
			{
				"category": "Ambitious",
				"column-1": "10.806476",
				"color": "#86a965"
			},
			{
				"category": "Shared Interests",
				"column-1": "11.832126",
				"color": "#8aabb0"
			}
		]
	}
);