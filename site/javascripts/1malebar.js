AmCharts.makeChart("1malebar",
	{
		"type": "serial",
		"categoryField": "category",
		"startDuration": 1,
		"theme": "black",
		"categoryAxis": {
			"gridPosition": "start"
		},
		"trendLines": [],
		"graphs": [
			{
				"colorField": "color",
				"fillAlphas": 1,
				"id": "AmGraph-1",
				"lineColorField": "color",
				"title": "graph 1",
				"type": "column",
				"valueField": "column-1"
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"title": "Importance Rating"
			}
		],
		"allLabels": [],
		"balloon": {},
		"titles": [
			{
				"id": "Title-1",
				"size": 15,
				"text": "Avg. Trait Importance Ratings (Male)"
			}
		],
		"dataProvider": [
			{
				"category": "Ambitious",
				"column-1": "8.823956",
				"color": "#de4c4f"
			},
			{
				"category": "Attractive",
				"column-1": "27.2488",
				"color": "#d8854f"
			},
			{
				"category": "Fun",
				"column-1": "17.600839",
				"color": "#eea638"
			},
			{
				"category": "Intelligent",
				"column-1": "16.3796",
				"color": "#a7a737"
			},
			{
				"category": "Shared Interests",
				"column-1": "10.982271",
				"color": "#86a965"
			},
			{
				"category": "Sincere",
				"column-1": "19.387418",
				"color": "#8aabb0"
			}
		]
	}
);