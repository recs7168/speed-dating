AmCharts.makeChart("2overall",
				{
					"type": "serial",
					"categoryField": "category",
					"startDuration": 1,
					"theme": "black",
					"categoryAxis": {
						"gridPosition": "start"
					},
					"trendLines": [],
					"graphs": [
						{
							"colorField": "color",
							"fillAlphas": 1,
							"id": "AmGraph-1",
							"lineColorField": "color",
							"title": "graph 1",
							"type": "column",
							"valueField": "column-1"
						}
					],
					"guides": [],
					"valueAxes": [
						{
							"id": "ValueAxis-1",
							"title": "Rating"
						}
					],
					"allLabels": [],
					"balloon": {},
					"titles": [
						{
							"id": "Title-1",
							"size": 15,
							"text": "Trait Ratings"
						}
					],
					"dataProvider": [
						{
							"category": "Attractive",
							"column-1": "6.190411",
							"color": "#de4c4f"
						},
						{
							"category": "Sincere",
							"column-1": "7.175256",
							"color": "#d8854f"
						},
						{
							"category": "Intelligent",
							"column-1": "7.369301",
							"color": "#eea638"
						},
						{
							"category": "Fun",
							"column-1": "6.400599",
							"color": "#a7a737"
						},
						{
							"category": "Ambitious",
							"column-1": "6.778409",
							"color": "#86a965"
						},
						{
							"category": "Shared Interests",
							"column-1": "5.474870",
							"color": "#8aabb0"
						}
					]
				}
			);