AmCharts.makeChart("1femalebyrace",
	{
		"type": "serial",
		"categoryField": "category",
		"angle": 30,
		"depth3D": 30,
		"startDuration": 1,
		"theme": "dark",
		"categoryAxis": {
			"gridPosition": "start"
		},
		"trendLines": [],
		"graphs": [
			{
				"balloonText": "[[title]] Rating from '[[category]]':[[value]]",
				"fillAlphas": 1,
				"id": "AmGraph-1",
				"title": "Attractive",
				"type": "column",
				"valueField": "Attractive"
			},
			{
				"balloonText": "[[title]] Rating from '[[category]]':[[value]]",
				"fillAlphas": 1,
				"fillColors": "#1D88A3",
				"id": "AmGraph-2",
				"lineColor": "#1D88A3",
				"title": "Intelligent",
				"type": "column",
				"valueField": "Intelligent"
			},
			{
				"balloonText": "[[title]] Rating from '[[category]]':[[value]]",
				"fillAlphas": 1,
				"id": "AmGraph-4",
				"title": "Fun",
				"type": "column",
				"valueField": "Fun"
			},
			{
				"balloonText": "[[title]] Rating from '[[category]]':[[value]]",
				"fillAlphas": 1,
				"id": "AmGraph-5",
				"title": "Sincere",
				"type": "column",
				"valueField": "Sincere"
			},
			{
				"balloonText": "[[title]] Rating from '[[category]]':[[value]]",
				"fillAlphas": 1,
				"id": "AmGraph-6",
				"title": "Ambitious",
				"type": "column",
				"valueField": "Ambitious"
			},
			{
				"balloonText": "[[title]] Rating from '[[category]]':[[value]]",
				"fillAlphas": 1,
				"id": "AmGraph-7",
				"title": "Shared Interests",
				"type": "column",
				"valueField": "Shared Interests"
			}
		],
		"guides": [],
		"valueAxes": [
			{
				"id": "ValueAxis-1",
				"title": "Rating"
			}
		],
		"allLabels": [],
		"balloon": {
			"animationDuration": 0
		},
		"legend": {
			"enabled": true,
			"maxColumns": 3,
			"rollOverGraphAlpha": 0,
			"useGraphSettings": true
		},
		"titles": [
			{
				"id": "Title-1",
				"size": 15,
				"text": "Trait Importance Ratings (Females by Race)"
			}
		],
		"dataProvider": [
			{
				"category": "Asian",
				"Attractive": "17.531972",
				"Intelligent": "19.517887",
				"Fun": "16.551549",
				"Sincere": "20.442394",
				"Ambitious": "12.059014",
				"Shared Interests": "13.898028"
			},
			{
				"category": "Black",
				"Attractive": "19.350625",
				"Intelligent": "16.060000",
				"Fun": "17.721250",
				"Sincere": "18.783750",
				"Ambitious": "15.580625",
				"Shared Interests": "12.504375"
			},
			{
				"category": "Latino",
				"Attractive": "19.278400",
				"Intelligent": "18.715600",
				"Fun": "15.863600",
				"Sincere": "22.465200",
				"Ambitious": "12.415200",
				"Shared Interests": "11.107500"
			},
			{
				"category": "Other",
				"Attractive": "18.452500",
				"Intelligent": "19.275625",
				"Fun": "16.518125",
				"Sincere": "20.520625",
				"Ambitious": "11.275625",
				"Shared Interests": "13.958125"
			},
			{
				"category": "White",
				"Attractive": "17.843262",
				"Intelligent": "17.608156",
				"Fun": "17.970780",
				"Sincere": "21.271560",
				"Ambitious": "13.134043",
				"Shared Interests": "12.243121"
			}
		]
	}
);